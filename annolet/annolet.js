
var json_data;          

getJsondata();

function getJsondata()
{
   disableAllLinks() // disables all the links in a page
   var url = "https://gl.githack.com/MSSoumya/thesis-demo-ren-ui/-/raw/main/annolet/config.json"; 
   $.getJSON(url, function() {
       console.log("Request to config file success..!");
   })
   .done(function(data, jqxhr, textStatus) {
       json_data = data;
       console.log("Retrieved the config data successfully..!");
       annoletContainer();
       addClickevents();
    })
   .fail(function(jqxhr, textStatus, error) {
       var err = textStatus + ", " + error;
       console.log("Request to config file failed: " + ", " + err);
    })
   .always(function() {
       console.log("Request to config_file complete");
    });
}

function annoletContainer(){
   //appending a div(annolet container) to body element of a webpage.
   var body = document.getElementsByTagName("body")[0];
   container = document.createElement("div");
   container.id = "annolet-container";
   container.className = "anno";
   body.appendChild(container);

   //appending a CSS stylesheet into head element of a webpage, which is used to stylize the annolet container.
   var linktag = document.createElement("link");
   linktag.rel = "stylesheet";
   linktag.type = "text/css";
   linktag.href = json_data.css["annolet"]; 
   document.getElementsByTagName('head')[0].appendChild(linktag);

   //injecting html code
   container.innerHTML = "<h4 id='annolet-header' class='anno'>Renarration</h4>"+
   "<ul id='annolet-tools-menu' class='anno'>"+
       "<li id='disable_list' class='annolet-menu-item anno'>"+
           "<button id='disable-css' class='annolet-menu-sub-item anno' title='Click on the button to view the page without css'>No CSS</button>"+
       "</li>"+
       "<li id='activezapper_list' class='annolet-menu-item anno'  title='Click on the button first and then point out the elements to remove the clutter'>"+
           "<button id='activate-zapper' class='annolet-menu-sub-item anno' >Activate Zapper</button>"+
       "</li>"+
       "<li id='deactivezapper_list' class='annolet-menu-item anno'>"+
           "<button id='deactivate-zapper' class='annolet-menu-sub-item anno' title='Click on the button to deactivate the zapper'>Deactivate Zapper</button>"+
       "</li>"+
       "<li id='modify_list' class='annolet-menu-item anno'  title='Click on the button first and then modify the content on a page' >"+
           "<button id='modify-content' class='annolet-menu-sub-item anno' >Modify Content</button>"+
       "</li>"+
       "<li id='highlighter_list' class='annolet-menu-item anno'  title='Select the text first and then click on the button to highlight the text' >"+
           "<button id='highlighter-btn' class='annolet-menu-sub-item anno' >Highlighter</button>"+
       "</li>"+
       "<li id='phonetics_list' class='annolet-menu-item anno'  title='Select the word to get translated and then click on the button'>"+
           "<button id='phonetics-btn' class='annolet-menu-sub-item anno' >Phonetics</button>"+
       "</li>"+
       "<li class='annolet-menu-item anno' title='Select the choice from the dropdown, select the text and then click on the button to get the translation for the selected text'>"+
           "<button id='trans-text' class='annolet-menu-sub-item anno' >Translate Text</button>"+"<br>"+
           "<select class='select-tools-menu anno' id='select-from-lang'>"+
               "<option value='en' class='anno'>English</option>"+
               "<option value='hi' class='anno'>Hindi</option>"+
               "<option value='te' class='anno'>Telugu</option>"+
               "<option value='ta' class='anno'>Tamil</option>"+
               "<option value='ml' class='anno'>Malayalam</option>"+
               "<option value='ja' class='anno'>Japanese</option>"+
               "<option value='zh' class='anno'>Chinese</option>"+
           "</select>"+
           "<select class='select-tools-menu anno' id='select-to-lang'>"+
               "<option value='zh-cn' class='anno'>Chinese</option>"+
               "<option value='ja' class='anno'>Japanese</option>"+
               "<option value='ml' class='anno'>Malayalam</option>"+
               "<option value='ta' class='anno'>Tamil</option>"+
               "<option value='te' class='anno'>Telugu</option>"+
               "<option value='hi' class='anno'>Hindi</option>"+
               "<option value='en' class='anno'>English</option>"+
           "</select>"+
        "</li>"+
        "<li class='annolet-menu-item anno' title='Select the theme and then click on button'>"+
            "<button id='change-theme' class='annolet-menu-sub-item anno'>Switch CSS</button>"+"<br>"+
            "<select class='select-tools-menu anno' id='select-theme'>"+
                "<option value='switch1' class='anno'>Theme1</option>"+
                "<option value='switch2' class='anno'>Theme2</option>"+
            "</select>"+
        "</li>"+
        "<li class='annolet-menu-item anno' title='Click on the button to view the links/images/text on a page by selecting the option from the dropdown'>"+
            "<button id='change-content' class='annolet-menu-sub-item anno'>Page Stripper</button>"+"<br>"+
            "<select class='select-tools-menu anno' id='select-content'>"+
                "<option class='anno' value='show-links' >Show Links</option>"+
                "<option class='anno' value='show-images' >Show Images</option>"+
                "<option class='anno' value='show-text'>Show Text</option>"+
            "</select>"+
        "</li>"+
        "<li class='annolet-menu-item anno' title='Select the choices from the dropdown, select the currency on page and then click on the button to translate the selected currency'>"+
            "<button id='change-currency' class='annolet-menu-sub-item anno' >Convert Currency</button>"+"<br>"+
            "<select class='select-tools-menu anno' id='select-from-currency'>"+
                "<option class='anno' value='USD' >USD</option>"+
                "<option class='anno' value='INR' >INR</option>"+
                "<option class='anno' value='EUR' >EUR</option>"+
            "</select>"+
            "<select class='select-tools-menu anno' id='select-to-currency'>"+
                "<option class='anno' value='INR' >INR</option>"+
                "<option class='anno' value='USD' >USD</option>"+
                "<option class='anno' value='EUR' >EUR</option>"+
            "</select>"+
        "</li>"+
        "<li class='annolet-menu-item anno' title='Select the choices from the dropdown, select the units on page and then click on the button to translate the selected unit'>"+
            "<button id='change-measurement' class='annolet-menu-sub-item anno' >Convert Measurements</button>"+"<br>"+
            "<select class='select-tools-menu anno' id='select-from-measure'>"+
                "<option class='anno' value='km'>kilometers</option>"+
                "<option class='anno' value='mi'>Miles</option>"+
                "<option class='anno' value='ft'>foot</option>"+
            "</select>"+
            "<select class='select-tools-menu anno' id='select-to-measure'>"+
                "<option class='anno' value='mi'>Miles</option>"+
                "<option class='anno' value='km'>kilometers</option>"+
                "<option class='anno' value='ft'>foot</option>"+
            "</select>"+
        "</li>"+
        "<li class='annolet-menu-item anno' title='Select the choice of format from the dropdown, select the num on page and then click on a button to format the selected num'>"+
            "<button id='change-num-sys' class='annolet-menu-sub-item anno' >Number Format</button>"+"<br>"+
            "<select class='select-tools-menu anno' id='select-num-sys'>"+
                "<option class='anno' value='en-IN' >Indian</option>"+
                "<option class='anno' value='en-US' >US</option>"+
                "<option class='anno' value='en-GB'>British</option>"+
                "<option class='anno' value='ko-KR'>Korean</option>"+
                "<option class='anno' value='ar-EG'>Arabic</option>"+
            "</select>"+
        "</li>"+
        "<li class='annolet-menu-item anno' title='Select the choice of format from the dropdown, select the date on page and then click on a button to format the selected date'>"+
            "<button id='change-date-format' class='annolet-menu-sub-item anno' >Date Format</button>"+"<br>"+
            "<select class='select-tools-menu anno' id='select-date-format'>"+
                "<option class='anno' value='en-IN' >Indian</option>"+
                "<option class='anno' value='en-US' >US</option>"+
                "<option class='anno' value='en-GB'>British</option>"+
                "<option class='anno' value='ko-KR'>Korean</option>"+
                "<option class='anno' value='ar-EG'>Arabic</option>"+
            "</select>"+
        "</li>"+
        "<li class='annolet-menu-item anno' title='Select the choice of visibility from dropdown and then click on the button to increase/decrease the font of a page'>"+
            "<button id='change-font' class='annolet-menu-sub-item anno' >Visibility</button>"+"<br>"+
            "<select class='select-tools-menu anno' id='select-font'>"+
                "<option class='anno' value='increase-font' >Increase Font</option>"+
                "<option class='anno' value='decrease-font' >Decrease Font</option>"+
            "</select>"+
        "</li>"+
   "</ul>";

    setTimeout(function() {
        alert("Page is ready to Renarrate");
    }, 1000);    
}

// this functionality runs on loading the bookmarklet
function disableAllLinks(){
   var anchors = document.getElementsByTagName("a");
   for( var i = 0, max = anchors.length; i < max; i++ ) {
       anchors[i].onclick = function() {return(false);};
   }
}

// this functionality runs on loading the bookmarklet
function disableCss(){
   // loops through all the external stylesheets and disables it
   var style_sheets = document.styleSheets;
   for( var i = 0, max = style_sheets.length; i < max; i++ ) {
      if(style_sheets[i].href === json_data.css["annolet"] ) {
         style_sheets[i].disabled = false;
      }
      else {
         style_sheets[i].disabled = true;
      }
   }
    
   // loops through all the body elems and disables the inline css
   var body_elems = document.body.getElementsByTagName("*");
   for( var i = 0, max = body_elems.length; i < max; i++ ) {
      var style_attr = $(body_elems[i]).attr("style");
      if(style_attr) {
         $(body_elems[i].tagName).removeAttr("style");
      }
   }

   // loops through all the head elems and disables the internal css
   var head_elems = document.head.getElementsByTagName("*");
   for( var i = 0, max = head_elems.length; i < max; i++ ) {
       if(head_elems[i].tagName === "STYLE") {
          $(head_elems[i].tagName).remove();
       }
   }
}

//function for the activate Zapper button
function activateZapPer(){
   $("body").click(function(event){
      var target_elem = event.target;
      if( target_elem.classList.contains("anno") === true ){
          target_elem.style.visibility = "visible";
      }
      else {
          target_elem.style.visibility = "hidden";
      }
   });
}

// function for the deactivate zapper button
function deactivateZapPer() {
   alert("Zapper deactivated");
   $('body').unbind("click");
   $('activate-zapper').unbind("click");   
}

// function for the modify content button
function modifyContent() {
   var all = document.getElementsByTagName("*");
   for(var i = 0, max = all.length; i < max; i++) { 
      if(all[i].classList.contains("anno") === false) {
         all[i].setAttribute("contenteditable", true);
         all[i].setAttribute("title", "Modify Content");
      }
   }
}

// function for the highlight button
function annoHighlight() {
   var current = getSelection();
   if(current !== "") {
      highlightContent(current);
   }
   else if(current === "") {
      alert("Select text and click on highlighter button");
   }
}

// Function for the phonetics button
function phoneticsTrans(){
   var current = getSelection();
   if(current !== "") {
      var url = json_data.api["phonetics-trans"];
      var xhr = new XMLHttpRequest();
      xhr.open("POST", url, true);
      xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
      xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
      xhr.send(JSON.stringify( {"sentence" : current.toString()} ));
      xhr.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200) {
           var res = this.responseText;
           console.log(res);
           if(res === current.toString()) {
             alert("Phonetics for the selected word is not available");
           }
           else {
             replaceText(res);
           }
        }
      }
   }
   else if(current === "") {
      alert("Select the word and click on 'Phonetics' button");
   }
}

// Function for the translate text button
function langTrans(){
   var from_lang = document.getElementById("select-from-lang").value;
   var to_lang = document.getElementById("select-to-lang").value;
   var current = getSelection();
   if(current !== "") { 
      alert(current);
      var url = json_data.api["lang-trans"];
      var xhr = new XMLHttpRequest();
      xhr.open("POST", url, true);
      xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
      xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
      xhr.send(JSON.stringify( {"sentence" : current.toString(), "from-language" : from_lang, "to-language" : to_lang} ));
      xhr.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200) {
           var res = this.responseText;
           replaceText(res);
        }
      }
    }
    else if(current === "") {
        alert("Select the text and click on 'Translate Text' button");
    }
}

// Function for the switch CSS button
function alternateStylesheets(){
   //appending a CSS alternate stylesheets to head element of a webpage.
   var i= 0;
   var style_sheets = 2; 
   var css_themes =[ json_data.css["Theme1"], json_data.css["Theme2"] ];
   var link_title =[ "switch1", "switch2" ];

   for( var i = 0; i < style_sheets; i++ ){
       var linktag = document.createElement("link");
       linktag.rel  = "alternate stylesheet";
       linktag.type = "text/css";
       linktag.href = css_themes[i];
       linktag.title = link_title[i];
       head = document.getElementsByTagName('head')[0];
       head.appendChild(linktag);
    }
    var selected_theme = document.getElementById("select-theme").value;
    switchStyle(selected_theme)
}

function switchStyle(css_title)
{   
  var linktag = document.getElementsByTagName("link");
  for( var i = 0; i < linktag.length; i++ ) {
     if ((linktag[i].rel.indexOf( "stylesheet" ) != -1) &&linktag[i].title) {
        linktag[i].disabled = true ;
        if(linktag[i].title == css_title) {
           linktag[i].disabled = false ;
        }
     }
  }
}

// Function for the button Page-stripper
function showContent(){
   var all = document.body.getElementsByTagName("*");
   var selected_content = document.getElementById("select-content").value;
   if(selected_content === "show-links") {
      for( var i = 0, max = all.length; i < max; i++ ) {
          if(all[i].hasAttribute("href") === true || all[i].classList.contains("anno") === true) {
             all[i].style.visibility = "visible";
          }
          else {
             all[i].style.visibility = "hidden";
          }
      }
   }
   else if(selected_content === "show-text") {
      for( var i = 0, max = all.length; i < max; i++ ) {
          if(all[i].innerHTML || all[i].classList.contains("anno") === true) {
             all[i].style.visibility = 'visible';
          }
          else {
             all[i].style.visibility = 'hidden';
          }
      }
   }
   else if(selected_content === "show-images") {
      for( var i = 0, max = all.length; i < max; i++ ) {
          if(all[i].tagName === "IMG" || all[i].classList.contains("anno") === true) {
             all[i].style.visibility = 'visible';
          }
          else {
             all[i].style.visibility = 'hidden';
          }
      }
   }   
}

//Function for the button "Visibility"
function changeFont(){
   var fontSize = parseInt($("body").css("font-size"), 10);
   var selected_font = document.getElementById("select-font").value;
   if(selected_font === "increase-font"){
      fontSize += 1.5;
      $("body").css("font-size", fontSize+"px");
   }
   else if(selected_font === "decrease-font"){
      fontSize -= 1;
      $("body").css("font-size", fontSize+"px");
   }
}

// Function for the button Convert Currency
function currencyTrans() {
   var from_cur = document.getElementById('select-from-currency').value;
   var to_cur = document.getElementById('select-to-currency').value;
   var current = getSelectedText();
   var sel_text = current.replace(/\,/g,"");
   if(current !== "") {
      if(isNaN(sel_text) === false) {
         var url = json_data.api["currency-conv"];
         var currency = parseFloat(sel_text);
         var xhr = new XMLHttpRequest();
         xhr.open("POST", url, true);
         xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
         xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
         xhr.send(JSON.stringify( { "currency_amount" : currency, "from_cur" : from_cur, "to_cur" : to_cur } ));
         xhr.onreadystatechange = function() {
            if(this.readyState === 4 && this.status === 200) {
               var res = this.responseText; 
               replaceText(res);
            }
         }
      }
      else if(isNaN(sel_text) === true) {
         alert("select the currency");
      }
    }
    else if(current === "") {
       alert("Select the currency and click on 'Convert Currency' button");
    }
}

// Function for the button Convert measurements
function convertUnits(){
   var from_unit = document.getElementById('select-from-measure').value;
   var to_unit = document.getElementById('select-to-measure').value;
   var current = getSelectedText();
   var sel_text = current.replace(/\,/g,"");
   if(current !== "") {
      if(isNaN(sel_text) == false) {
         // var url = "//10.4.12.35:5000/measurement-conversion";
         var url = json_data.api["unit-conversion"]
         var units = parseFloat(sel_text);
         var xhr = new XMLHttpRequest();
         xhr.open("POST", url, true);
         xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
         xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
         xhr.send(JSON.stringify({ "measurement_num": units, "from_measure": from_unit, "to_measure": to_unit }));
         xhr.onreadystatechange = function() {
            if(this.readyState === 4 && this.status === 200) {
                var res = this.responseText;
                replaceText(res);                    
            }
         }
      }
      else if(isNaN(sel_text) === true) {
         alert("select units");
      }
   }
   else if(current === "") {
      alert("Select units and click on 'Convert Measurements' button");
   }
}

// Function for the button Number format
function numFormat() {
   var current = getSelectedText();
   var selected_numsys = document.getElementById("select-num-sys").value;
   var sel_text =  current.replace(/\,/g,"");
   if(current !== "") {
      if(isNaN(sel_text) === false) {
          var num_to_format = parseFloat(sel_text);
          var num_to_translate = num_to_format.toLocaleString(selected_numsys);
          replaceText(num_to_translate);
      }
      else if(isNaN(sel_text) === true) {
          alert("Invalid Number");
      }
   }
   else if(current === "") {
      alert("Select the number and click on 'Number format' button");
   }
}

// Function to replace the highlighted text based on the operation performed by the user.
function replaceText(replacementText) {
   var sel, range;
   if (window.getSelection) {
       sel = window.getSelection();
       if (sel.rangeCount) {
           range = sel.getRangeAt(0);
           range.deleteContents();
           range.insertNode(document.createTextNode(replacementText));
       }
   } else if (document.selection && document.selection.createRange) {
       range = document.selection.createRange();
       range.text = replacementText;
   }
   highlightContent(window.getSelection());
}

function formatDate(){
   var current = getSelectedText();
   var selected_format = document.getElementById("select-date-format").value;
   if(current != "") {
      var date_to_format = new Date(current);
      var formated_date = date_to_format.toLocaleDateString(selected_format);
      if(formated_date === "Invalid Date") {
          alert(formated_date);
      }
      else {
          highlightContent(formated_date);
      }
   }
   else if(current == "") {
      alert("Select the date and click on 'Date format' button");
   }
}

function getSelectedText(){ 
   if(window.getSelection){
      return window.getSelection().toString(); 
   } 
   else if(document.getSelection){
      return document.getSelection(); 
   } 
   else if(document.selection){
      return document.selection.createRange().text; 
   }
   return ''; 
}

function highlightContent(sel_text) {      
   var span = document.createElement("span");
   span.textContent = sel_text;
   span.style.backgroundColor = "yellow";
   span.id = "highlighted";
   span.className = "anno";
   var range = sel_text.getRangeAt(0);
   range.deleteContents();
   range.insertNode(span);
}

function addClickevents(){
   document.getElementById('disable-css').addEventListener('click', function() {
      disableCss()
   }, false);
   document.getElementById('activate-zapper').addEventListener('click', function() {
      activateZapPer()
   }, false);
   document.getElementById('deactivate-zapper').addEventListener('click', function() {
      deactivateZapPer()
   }, false);
   document.getElementById('modify-content').addEventListener('click', function() {
      modifyContent()
   }, false);
   document.getElementById('highlighter-btn').addEventListener('click', function() {
      annoHighlight()
   }, false);
   document.getElementById('phonetics-btn').addEventListener('click', function(event) {
      phoneticsTrans()
   }, false);
   document.getElementById('trans-text').addEventListener('click', function() {
      langTrans()
   }, false);
   document.getElementById('change-theme').addEventListener('click', function() {
      alternateStylesheets()
   }, false);
   document.getElementById('change-content').addEventListener('click', function() {
      showContent()
   }, false);
   document.getElementById('change-currency').addEventListener('click', function() {
      currencyTrans()
   }, false);
   document.getElementById('change-measurement').addEventListener('click', function() {
      convertUnits()
   }, false);
   document.getElementById('change-num-sys').addEventListener('click', function() {
      numFormat()
   }, false);
   document.getElementById('change-date-format').addEventListener('click', function() {
      formatDate()
   }, false);
   document.getElementById('change-font').addEventListener('click', function() {
      changeFont()
   }, false);
}


